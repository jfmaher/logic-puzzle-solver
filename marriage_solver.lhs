> import Data.List

Here are the categories are defined.

> data Man = Paul | Rob | Stan | Vern | Wally 
>     deriving (Eq, Show, Enum, Bounded)
> data Woman = Anne | Cathy | Eve | Fran | Ida
>     deriving (Eq, Show, Enum, Bounded)

This type alias is used to decribe a day. That has a list of women and men which will be who can still be considered married at that day.
	 
> type Day a b = ([a], [b])

Showweek puts new lines inbetween in the the Days in a list of Days

> showweek' :: (Show a, Show b) => [Day a b] -> String
> showweek' [] = []
> showweek' ((alist, blist):tail) =  (show alist) ++ (show blist) ++ "\n" ++ (showweek' tail)

> showweek :: (Show a, Show b) => [Day a b] -> IO()
> showweek = putStr . showweek'

These functions are used to define the week with all the possibilities which is defined as theweek.

It uses the Bound typeclass as a shorthand in men and women.

> men = [minBound ..] :: [Man]
> women = [minBound ..] :: [Woman]
> theweek = take 5 $ repeat (men, women)

These fuctions work the direct relations. They place an element at a position in the week given by the int. While traversing the week it also removes the given element from the other its other current possibilities. This is just an element of convienience and allows the total action to occur in the single fuction.

These fuctions can be used to fix values in the table. This ability is used when applying relative rules.

> on1 :: Eq a => a -> Int -> [Day a b] -> [Day a b]
> on1 _ _ [] = []
> on1 man index ((manlist, wm):t)  = ((if index == 0 
>                                       then ([man], wm) 
>                                       else (delete man manlist, wm))
>                                     : on1 man (index - 1) t)

> on2 :: Eq b => b -> Int -> [Day a b] -> [Day a b]
> on2 _ _ [] = []
> on2 woman index ((mn, wm):t)  = ((if index == 0
>                                       then (mn, [woman]) 
>                                       else (mn, delete woman wm))
>                                     : on2 woman (index - 1) t)

The Val will be used by the rules to indictate if the contained element in the rule is a negation or is meant to be there. T signafying if it sould be in a place and F if it should not.

> data Val a = T a | F a
>      deriving (Show)

The pair function enforces the given rule, given by the two Vals passed to it, for each Day in a list of days. It returns the ammended list of days. These functions are composable so the rules can be combined into a single function using compisition because the last argument and return type are the same. There are four implementations for pair corresponding with the possible dataconstructors for Val.

> pair :: (Eq a, Eq b) => Val a -> Val b -> [Day a b] -> [Day a b]
> pair _ _ [] = []

This implementation takes both elements in the rule to be true.

The guards check if one of the Vals is the last in its respective list. If it is the other element must be the only element of the list so it is placed in. 

> pair va@(T a) vb@(T b) ((alist, blist):tail) | alist == [a] = (([a],[b]):tail) -- Check for b TODO
>                                              | blist == [b] = (([a],[b]):tail) -- Check for a TODO

Also if one is not present it means that the other cannot be on the same day. This is checked by seeing if the element is in its list and if not removing the other. If both are still present the lists are not changed.

In the if statements logical implies is equivalent to the less than or equal on Bools because Bools are an instance of the Ord class. This means that False < True.

>                                              | otherwise    = if aval <= bval
>                                                                 then if bval <= aval
>                                                                         then ((alist, blist):pair va vb tail)
>                                                                         else ((alist, delete b blist):pair va vb tail)
>                                                                 else ((delete a alist, blist):pair va vb tail)
>                                                                     where   aval = elem a alist
>                                                                             bval = elem b blist

For these implementations if one is present on its own , a final answer, the other is removed. This is easily implemented through the gaurds.                                            

> pair va@(T a) vb@(F b) ((alist, blist):tail) | alist == [a] = (([a],delete b blist): pair va vb tail)
>                                              | blist == [b] = ((delete a alist,[b]): pair va vb tail)
>                                              | otherwise    = ((alist, blist): pair va vb tail)
                                            
> pair va@(F a) vb@(T b) ((alist, blist):tail) | alist == [a] = (([a],delete b blist): pair va vb tail)
>                                              | blist == [b] = ((delete a alist,[b]): pair va vb tail)
>                                              | otherwise    = ((alist, blist): pair va vb tail)

If one is not present the other cannot. This is again simply implemented through guards.

> pair va@(F a) vb@(F b) ((alist, blist):tail) | not $ elem a alist = ((alist, delete b blist):tail)
>                                              | not $ elem b blist = ((delete a alist, blist):tail)
>                                              | otherwise    = ((alist, blist): pair va vb tail)

The solved function is used the elements that have been solved so far. This means that they will be the last element of their type on a particular day. They are returned in the format of a Day a b

> solved [] = ([], [])                                             
> solved ((alist, blist):tail) | length alist == 1 && length blist == 1 = (alist ++ al, blist ++ bl)
>                              | length alist == 1 = (alist ++ al, bl)
>                              | length blist == 1 = (alist, blist ++ bl)
>                              | otherwise      = (al, bl)
>                                        where (al, bl) = solved tail

Propagate removes the elements its given to it in the format of a Day a b from everyday where they are not the only element.

> propagate _ [] = []
> propagate rm@(rmalist, rmblist) ((alist, blist):tail)  | length alist == 1 && length blist == 1 = ((alist, blist): propagate rm tail)
>                                                        | length alist == 1 = ((alist, filter (\b -> notElem b rmblist) blist): propagate rm tail)
>                                                        | length blist == 1 = ((filter (\a -> notElem a rmalist) alist, blist): propagate rm tail)
>                                                        | otherwise         = ((filter (\a -> notElem a rmalist) alist, 
>                                                                                filter (\b -> notElem b rmblist) blist): propagate rm tail)

The true use of the last two functions is to combine them so the elements that have certain days, they are the only ones left are removed from the rest of the days that they are still present.

> propagateSolved x = propagate (solved x) x

This function continually applies a function until the result no longer changes.

> repeatedApply funct days = let newdays = funct days 
>                            in if newdays == days
>                               then propagateSolved newdays
>                               else repeatedApply funct $ propagateSolved newdays

The relative rules are applied as a function as are the pair rules but they are not compositional because they cannot input their result. This means that they will have to be treated differently to pair and direct rules.

The relative rules are represented similar to the other rules but have an offset because they represent relations between different days. The checkRelation returns True if the relation is true for a particular day. The insertRelation creates new tables from the one passed in while fixing the relation's values for a given index. 

> relative :: (Eq a, Eq b) => Val a -> Val b -> Int -> [Day a b] -> [[Day a b]]
> relative _ _ _ [] = []
> relative va vb offset week = relative' va vb (take (length week) [0..]) offset week
> relative' (T a) (T b) indices offset week = map insertRelation (filter checkRelation indices)
>                                             where
>                                                 checkRelation index | index + offset >= 0 &&
>                                                                       index + offset <= length week = case week !! index of
>                                                                                                            (alist, _) -> if elem a alist
>                                                                                                                          then case week !! (index + offset) of 
>                                                                                                                                    (_, blist) -> if elem b blist
>                                                                                                                                                  then True
>                                                                                                                                                  else False
>                                                                                                                          else False
>                                                                     | otherwise = False
>                                                 insertRelation i = on1 a i $ on2 b (i + offset) week

These are the rules for the Marriage Puzzle in the format of the solver. Note that relatives is a list because they are not compositional and they are better applied one at a time.

> directs = on2 Anne 0 . on1 Stan 2 . on1 Rob 4
> pairs = pair (F Wally) (T Anne) . pair (T Vern) (T Fran) . pair (T Rob) (F Ida)
> relatives = [relative (T Vern) (T Eve) (-1)]

iterateRel is the most important funtion for solving. It applies the pair and relative rules until they stop reducing the table.

In the let afterIterate the gap between the relative function returning [[Day a b]] and the pair function returning [Day a b] is bridged by using concat to combine the new list of lists of tables generated by the map . relative to a list of tables that can have pairs mapped to them.

> iterateRel :: (Eq a, Eq b) => ([Day a b] -> [Day a b]) -> ([[Day a b] -> [[Day a b]]]) -> [[Day a b]] -> [[Day a b]]
> iterateRel _ [] weeks = weeks
> iterateRel pairs (relative:others) weeks = let afterIterate = map (repeatedApply pairs) $ concat $ map relative weeks
>                                            in if afterIterate == weeks
>                                               then afterIterate
>                                               else iterateRel pairs others afterIterate

The solve function starts the solver by appling the direct rules and passing the result to the iterateRel function.

> solve :: (Eq a, Eq b) => ([Day a b] -> [Day a b]) -> ([Day a b] -> [Day a b]) -> ([[Day a b] -> [[Day a b]]]) -> [Day a b] -> [[Day a b]]
> solve directs pairs relatives week = let afterDirects = directs week
>                                      in Main.iterateRel pairs relatives [afterDirects]

The result of the example.

> solveMarriage = solve directs pairs relatives theweek
