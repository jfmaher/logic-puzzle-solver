types = ["color","man","pet","drink","smoke"]

format= "({a},{b},{c},{d},{e}) >=<> (aL,bL,cL,dL,eL) = (({a2}aL),({b2}bL),({c2}cL),({d2}dL),({e2}eL))\n"

def elem(offset):
    return "["+chr(ord('a')+offset)+"]"

def head(offset):
    return chr(ord('a')+offset)+":"

def calcElem(iter, base, offset):
    if (iter//base) % 2 > 0:
        return elem(offset)
    else:
        return "(_:_)"

def calcHead(iter, base, offset):
    if (iter//base) % 2 > 0:
        return head(offset)
    else:
        return ""

def genFunct(lines):
    s = ""
    for i in range(lines):
        s += format.format(a=calcElem(i,16,0),b=calcElem(i,8,1),c=calcElem(i,4,2),d=calcElem(i,2,3),e=calcElem(i, 1, 4),a2=calcHead(i,16,0),b2=calcHead(i,8,1),c2=calcHead(i,4,2),d2=calcHead(i,2,3),e2=calcHead(i,1,4))
    return s

formGetable = "instance Getable {captype} where\n    get {tuple} = {type}s"

def genGetTup(typ, index, total):
        s="("
        for i in range(total):
            if i == index:
                s+=typ+","
            else:
                s+=" _ ,"
        return s[0:len(s)-1]+")"
	
def getGetables(types):
	s=""
	for typ in types:
		s += formGetable.format(captype=typ.capitalize(), tuple=genGetTup(typ+"s",types.index(typ),len(types)), type=typ)+"\n"
	return s

formInsertable = "instance Insertable {type} where\n    insert {insert} {gettup} = {instup}"

def genInsTup(index, types):
    s="("
    for i in range(len(types)):
        if i != index:
            s+=types[i]+","
        else:
            s+="_,"
    return s[0:len(s)-1]+")"

def genInsertedTup(ins, index, types):
    s="("
    for i in range(len(types)):
        if i != index:
            s+=types[i]+","
        else:
            s+=ins+ ","
    return s[0:len(s)-1]+")"

def getInsertables(types):
    s=""
    for typ in types:
        s+=formInsertable.format(type=typ.capitalize(), insert=typ+"Ins", gettup=genInsTup(types.index(typ),types), instup=genInsertedTup(typ+"Ins",types.index(typ),types))+"\n"
    return s


if __name__=='__main__': print(genFunct(32)+"\n\n"+getGetables(types)+"\n\n"+getInsertables(types))
