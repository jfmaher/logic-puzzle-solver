import Data.List hiding (insert)

compose :: [a -> a] -> a -> a
compose fs = foldl (flip (.)) id fs --From haskell.org/haskellwiki/Compose 

ppList :: Show a => [a] -> IO()
ppList = putStr . ppList'
ppList' [] = []
ppList' (head:tail) = (show head) ++ "\n" ++ (ppList' tail)

data Colour = Red | Green | Blue | Yellow | White
    deriving (Eq, Show, Enum, Bounded)
 
data Man = Eng | Swe | Dan | Nor | Ger
    deriving (Eq, Show, Enum, Bounded)
 
data Pet = Dog | Birds | Cats | Horse | Zebra
    deriving (Eq, Show, Enum, Bounded)
 
data Drink = Coffee | Tea | Milk | Beer | Water
    deriving (Eq, Show, Enum, Bounded)
 
data Smoke = PallMall | Dunhill | Blend | BlueMaster | Prince
    deriving (Eq, Show, Enum, Bounded)
    
type Container = ([Colour],[Man],[Pet],[Drink],[Smoke])
emptyCont = ([],[],[],[],[])

colours = [minBound..] :: [Colour]
men = [minBound..] :: [Man]
pets = [minBound..] :: [Pet]
drinks = [minBound..] :: [Drink]
smokes = [minBound..] :: [Smoke]
theStreet = take 5 $ repeat (colours,men,pets,drinks,smokes)

solved :: Container -> Container
solved (a,b,c,d,e) = let v = ifSingle a
                     in let w = ifSingle b
                        in let x = ifSingle c
                           in let y = ifSingle d
                              in let z = ifSingle e
                                 in (\v w x y z -> (v,w,x,y,z)) v w x y z
                     where
                        ifSingle [el] = [el]
                        ifSingle _ = []
                        
contUnion :: Container -> Container -> Container
contUnion (a,b,c,d,e) (w,v,x,y,z) = (a++w,b++v,c++x,d++y,e++z)

findSolved [] = emptyCont
findSolved (cont:tail) = solved cont `contUnion` findSolved tail

propagate :: Container -> [Container] -> [Container]
propagate (aList, bList, cList, dList, eList) = compose (concat [map safeRemove aList, map safeRemove bList, map safeRemove cList, map safeRemove dList, map safeRemove eList])

propagateSolved x = propagate (findSolved x) x

class Contained a where
    get :: Container -> [a]
    insert :: [a] -> Container -> Container
instance Contained Colour where
    get (colors, _ , _ , _ , _ ) = colors
    insert colorIns (_,man,pet,drink,smoke) = (colorIns,man,pet,drink,smoke)
instance Contained Man where
    get ( _ ,mans, _ , _ , _ ) = mans
    insert manIns (color,_,pet,drink,smoke) = (color,manIns,pet,drink,smoke)
instance Contained Pet where
    get ( _ , _ ,pets, _ , _ ) = pets
    insert petIns (color,man,_,drink,smoke) = (color,man,petIns,drink,smoke)
instance Contained Drink where
    get ( _ , _ , _ ,drinks, _ ) = drinks
    insert drinkIns (color,man,pet,_,smoke) = (color,man,pet,drinkIns,smoke)
instance Contained Smoke where
    get ( _ , _ , _ , _ ,smokes) = smokes
    insert smokeIns (color,man,pet,drink,_) = (color,man,pet,drink,smokeIns) 

remove :: (Contained a, Eq a) => a -> Container -> Container
remove el cont = insert (delete el (get cont)) cont

safeRemove :: (Contained a, Eq a) => a -> [Container] -> [Container]
safeRemove _ [] = []
safeRemove a (cont:tail) | get cont == [a] = (cont: safeRemove a tail)
                         | otherwise       = (remove a cont: safeRemove a tail)
						 
direct :: (Contained a, Eq a) => (Val a) -> Int -> [Container] -> [Container]
direct _ _ [] = []
direct (T el) index (cont:t)  = ((if index == 0 
                                   then insert [el] cont 
                                   else remove el cont)
                                  : direct (T el) (index - 1) t)
                              
direct (F el) index (cont:t)  = ((if index == 0 
                                   then remove el cont 
                                   else cont)
                                  : direct (F el) (index - 1) t)
                              
data Val a = T a | F a
     deriving (Show)
     
pair :: (Contained a, Eq a, Contained b, Eq b) 
            => Val a -> Val b -> [Container] -> [Container]
pair _ _ [] = []
pair va@(T a) vb@(T b) (cont:tail) | get cont == [a] = (insert [b] cont:tail) -- Check for b TODO
                                   | get cont == [b] = (insert [a] cont:tail) -- Check for a TODO
                                   | otherwise       = if aval <= bval
                                                                then if bval <= aval
                                                                        then (cont:pair va vb tail)
                                                                        else (remove b cont:pair va vb tail)
                                                                else (remove a cont:pair va vb tail)
                                                                    where   aval = elem a $ get cont
                                                                            bval = elem b $ get cont
                                            
pair va@(T a) vb@(F b) (cont:tail) | get cont == [a] = (remove b cont: pair va vb tail)
                                   | get cont == [b] = (remove a cont: pair va vb tail)
                                   | otherwise       = (cont: pair va vb tail)
                                            
pair va@(F a) vb@(T b) (cont:tail) | get cont == [a] = (remove b cont: pair va vb tail)
                                   | get cont == [b] = (remove a cont: pair va vb tail)
                                   | otherwise       = (cont: pair va vb tail)

pair va@(F a) vb@(F b) (cont:tail) | notInCont a = (remove b cont:tail)
                                   | notInCont b = (remove a cont:tail)
                                   | otherwise   = (cont: pair va vb tail)
                                     where notInCont el = not $ elem el $ get cont
                                     
--Remains a single table.
relative :: (Contained a, Eq a, Contained b, Eq b) 
                => Val a -> Val b -> Int -> [Container] -> [Container]
relative _ _ _ [] = []
relative va@(T a) vb@(T b) offset week | offset == 0 = pair va vb week
                                       | offset > 0  = let notOverlap = applyFunctionList (map (\x -> direct (F b) x) [0..(offset-1)]) week
                                                           in relCheck notOverlap
                                           where
                                               applyFunctionList [] a       = a
                                               applyFunctionList (f:tail) a = f (applyFunctionList tail a)
                                               relCheck [] = []
                                               relCheck (cont:tail) | length tail < offset = (remove a cont:tail)
                                                                    | get cont == [a] = (cont:direct (T b) (offset-1) tail)
                                                                    | otherwise       = if aval <= bval
                                                                                            then if bval <= aval
                                                                                                    then (cont: relCheck tail)
                                                                                                    else (cont:(relCheck $ direct (F b) (offset-1) tail))
                                                                                            else (remove a cont:relCheck tail)
                                                                                                where   aval = elem a $ get cont
                                                                                                        bval = elem b $ get (tail !! (offset - 1))
                                                                                                        
nextTo :: (Contained a, Eq a, Contained b, Eq b) 
                => Val a -> Val b -> [Container] -> [Container]
nextTo va vb week = nextTo' va vb week 0
nextTo' va@(T a) vb@(T b) week index | index == length week = week
                                     | (get cont) == [a] = case (leftCheck b, rightCheck b) of
                                                                          (True,True)  -> nextTo' va vb week (index + 1)
                                                                          (True,False) -> nextTo' va vb (direct (T b) (index-1) week) (index + 1)
                                                                          (False,True) -> nextTo' va vb (direct (T b) (index+1) week) (index + 1)
                                     | (get cont) == [b] = case (leftCheck a, rightCheck a) of
                                                                          (True,True)  -> nextTo' va vb week (index + 1)
                                                                          (True,False) -> nextTo' va vb (direct (T a) (index-1) week) (index + 1)
                                                                          (False,True) -> nextTo' va vb (direct (T a) (index+1) week) (index + 1)
                                     | otherwise = case nextCheck of
                                                    (True,True)  -> nextTo' va vb week (index + 1)
                                                    (True,False) -> nextTo' va vb (direct (F b) index week) (index + 1)
                                                    (False,True) -> nextTo' va vb (direct (F a) index week) (index + 1)
                                                    _            -> nextTo' va vb (direct (F b) index $ direct (F a) index week) (index + 1)
                                      where 
                                          cont = week !! index
                                          next = week !! (index + 1)
                                          prev = week !! (index - 1)
                                          nextCheck  = (leftCheck b || rightCheck b, leftCheck a || rightCheck a)
                                          leftCheck :: (Contained el, Eq el) => el -> Bool
                                          leftCheck | index > 0 = \ el -> elem el $ get prev
                                                    | otherwise = \ el -> False
                                          rightCheck :: (Contained el, Eq el) => el -> Bool
                                          rightCheck | index < (length week) - 1 = \ el -> elem el $ get next
                                                     | otherwise                 = \ el -> False                                          
																										
solve directs repeates week = let afterDirects = directs week
                                         in iterate afterDirects
                                         where iterate week = let after = propagateSolved $ repeates week
                                                                  in if after == week
                                                                      then after
                                                                      else iterate after
                                                                      
directs = direct (T Milk) 2 . direct (T Nor) 0
pairs = pair (T Eng) (T Red) . pair (T Swe) (T Dog) . pair (T Dan) (T Tea) . pair (T Coffee) (T Green) . pair (T PallMall) (T Birds) . pair (T Yellow) (T Dunhill) . pair (T BlueMaster) (T Beer) . pair (T Ger) (T Prince)
relatives = relative (T Green) (T White) 1 . nextTo (T Blend) (T Cats) . nextTo (T Horse) (T Dunhill) . nextTo (T Nor) (T Blue) . nextTo (T Water) (T Blend)

solveZebra = solve directs (relatives.pairs) theStreet