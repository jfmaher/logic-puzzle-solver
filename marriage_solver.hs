import Data.List

compose :: [a -> a] -> a -> a
compose fs = foldl (flip (.)) id fs --From haskell.org/haskellwiki/Compose 

data Man = Paul | Rob | Stan | Vern | Wally 
     deriving (Eq, Show, Enum, Bounded)

data Woman = Anne | Cathy | Eve | Fran | Ida
     deriving (Eq, Show, Enum, Bounded)
	 
type Day a b = ([a], [b])

showweek' :: (Show a, Show b) => [Day a b] -> String
showweek' [] = []
showweek' ((alist, blist):tail) =  (show alist) ++ (show blist) ++ "\n" ++ (showweek' tail)

showweek :: (Show a, Show b) => [Day a b] -> IO()
showweek = putStr . showweek'

men = [minBound ..] :: [Man]
women = [minBound ..] :: [Woman]
theweek = take 5 $ repeat (men, women)

direct1 :: Eq a => a -> Int -> [Day a b] -> [Day a b]
direct1 _ _ [] = []
direct1 man index ((manlist, wm):t)  = ((if index == 0 
                                      then ([man], wm) 
                                      else (delete man manlist, wm))
                                    : direct1 man (index - 1) t)

direct2 :: Eq b => b -> Int -> [Day a b] -> [Day a b]
direct2 _ _ [] = []
direct2 woman index ((mn, wm):t)  = ((if index == 0
                                      then (mn, [woman]) 
                                      else (mn, delete woman wm))
                                    : direct2 woman (index - 1) t)

rm1 :: Eq a => a -> Int -> [Day a b] -> [Day a b]
rm1 _ _ [] = []
rm1 man index ((manlist, wm):t)  = ((if index == 0 
                                      then (delete man manlist, wm) 
                                      else (manlist, wm))
                                    : rm1 man (index - 1) t)

rm2 :: Eq b => b -> Int -> [Day a b] -> [Day a b]
rm2 _ _ [] = []
rm2 woman index ((mn, wm):t)  = ((if index == 0
                                      then (mn, delete woman wm) 
                                      else (mn, wm))
                                    : rm2 woman (index - 1) t)
                                    
data Val a = T a | F a
     deriving (Show)

pair :: (Eq a, Eq b) => Val a -> Val b -> [Day a b] -> [Day a b]
pair _ _ [] = []
pair va@(T a) vb@(T b) ((alist, blist):tail) | alist == [a] = (([a],[b]):tail)
                                             | blist == [b] = (([a],[b]):tail)
                                             | otherwise    = case (aval, bval) of 
                                                                (True, True)   -> ((alist, blist):pair va vb tail)
                                                                (True, False)  -> ((delete a alist, blist):pair va vb tail)
                                                                (False, True)  -> ((alist, delete b blist):pair va vb tail)
                                                                (False, False) -> ((delete a alist, delete b blist):pair va vb tail)
                                                              where   aval = elem a alist
                                                                      bval = elem b blist
                                            
pair va@(T a) vb@(F b) ((alist, blist):tail) | alist == [a] = (([a],delete b blist): pair va vb tail)
                                             | blist == [b] = ((delete a alist,[b]): pair va vb tail)
                                             | otherwise    = ((alist, blist): pair va vb tail)
                                            
pair va@(F a) vb@(T b) ((alist, blist):tail) | alist == [a] = (([a],delete b blist): pair va vb tail)
                                             | blist == [b] = ((delete a alist,[b]): pair va vb tail)
                                             | otherwise    = ((alist, blist): pair va vb tail)

pair va@(F a) vb@(F b) ((alist, blist):tail) | not $ elem a alist = ((alist, delete b blist):tail)
                                             | not $ elem b blist = ((delete a alist, blist):tail)
                                             | otherwise    = ((alist, blist): pair va vb tail)

solved [] = ([], [])                                             
solved ((alist, blist):tail) | length alist == 1 && length blist == 1 = (alist ++ al, blist ++ bl)                                      
                             | length alist == 1 = (alist ++ al, bl)
                             | length blist == 1 = (alist, blist ++ bl)
                             | otherwise      = (al, bl)
                                       where (al, bl) = solved tail

propagate _ [] = []
propagate rm@(rmalist, rmblist) ((alist, blist):tail)  | length alist == 1 && length blist == 1 = ((alist, blist): propagate rm tail)
                                                       | length alist == 1 = ((alist, filter (\b -> notElem b rmblist) blist): propagate rm tail)
                                                       | length blist == 1 = ((filter (\a -> notElem a rmalist) alist, blist): propagate rm tail)
                                                       | otherwise         = ((filter (\a -> notElem a rmalist) alist, 
                                                                               filter (\b -> notElem b rmblist) blist): propagate rm tail)


propagateSolved x = propagate (solved x) x
repeatedApply funct days = let newdays = funct days 
                           in if newdays == days
                              then propagateSolved newdays
                              else repeatedApply funct $ propagateSolved newdays


--Remains a single table.
relative _ _ _ [] = []
relative (T a) (T b) offset week = overlapRemove offset $ removeFalseRelation (filter (not.checkRelation) (take (length week) [0..]))
                                             where
                                                checkRelation index | index + offset >= 0 &&
                                                                      index + offset < length week = case week !! index of
                                                                                                           (alist, _) -> if elem a alist
                                                                                                                         then case week !! (index + offset) of 
                                                                                                                                   (_, blist) -> if elem b blist
                                                                                                                                                 then True
                                                                                                                                                 else False
                                                                                                                         else False
                                                                    | otherwise = False
                                                removeFalseRelation [] = week
                                                removeFalseRelation (index:tail) = rm1 a index $ rm2 b (index + offset) $ removeFalseRelation tail
                                                overlapRemove offset week | offset > 0 = compose (map (\x -> rm1 a x) [0..(offset-1)]) week
                                                                          | offset < 0 = compose (map (\x -> rm2 b x) [((length week) - 1)..((length week)-1-offset)]) week
                                                                          | otherwise = week
directs = direct2 Anne 0 . direct1 Stan 2 . direct1 Rob 4
pairs = pair (F Wally) (T Anne) . pair (T Vern) (T Fran) . pair (T Rob) (F Ida)
relatives = relative (T Vern) (T Eve) (-1)

solve directs pairs relatives week = let afterDirects = directs week
                                         in iterate afterDirects
                                         where iterate week = let after = propagateSolved $ relatives $ pairs week
                                                                  in if after == week
                                                                      then after
                                                                      else iterate after

solveMarriage = solve directs pairs relatives theweek