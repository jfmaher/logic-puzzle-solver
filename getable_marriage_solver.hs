import Data.List hiding (insert)

compose :: [a -> a] -> a -> a
compose fs = foldl (flip (.)) id fs --From haskell.org/haskellwiki/Compose 

data Man = Paul | Rob | Stan | Vern | Wally 
     deriving (Eq, Show, Enum, Bounded)

data Woman = Anne | Cathy | Eve | Fran | Ida
     deriving (Eq, Show, Enum, Bounded)
	 
type Container = ([Man], [Woman])
emptyCont = ([],[])

solved :: Container -> Container
solved (a, b) = let x = ifSingle a
                    in let y = ifSingle b
                        in (x, y)
                where
                    ifSingle [el] = [el]
                    ifSingle _    = []

contUnion (a1, b1) (a2, b2) = (a1++a2, b1++b2)

propagate :: Container -> [Container] -> [Container]
propagate (aList, bList) = compose (concat [map safeRemove aList, map safeRemove bList])

findSolved [] = emptyCont
findSolved (cont:tail) = solved `contUnion` (findSolved tail)

propagateSolved x = propagate (findSolved x) x

class Contained a where
    get :: Container -> [a]
    insert :: [a] -> Container -> Container
instance Contained Man where
    get (men,  _) = men
    insert manList (_, women) = (manList, women)
instance Contained Woman where
    get (_, women) = women
    insert womenList (men, _) = (men, womenList)
 
remove :: (Contained a, Eq a) => a -> Container -> Container
remove el cont = insert (delete el (get cont)) cont

safeRemove :: (Contained a, Eq a) => a -> [Container] -> [Container]
safeRemove _ [] = []
safeRemove a (cont:tail) | get cont == [a] = (cont: safeRemove a tail)
                         | otherwise       = (remove a cont: safeRemove a tail)

men = [minBound ..] :: [Man]
women = [minBound ..] :: [Woman]
theweek = take 5 $ repeat (men, women)

direct :: (Contained a, Eq a) => (Val a) -> Int -> [Container] -> [Container]
direct _ _ [] = []
direct (T el) index (cont:t)  = ((if index == 0 
                                   then insert [el] cont 
                                   else remove el cont)
                                  : direct (T el) (index - 1) t)
                              
direct (F el) index (cont:t)  = ((if index == 0 
                                   then remove el cont 
                                   else cont)
                                  : direct (F el) (index - 1) t)
                              
data Val a = T a | F a
     deriving (Show)
     
pair :: (Contained a, Eq a, Contained b, Eq b) 
            => Val a -> Val b -> [Container] -> [Container]
pair _ _ [] = []
pair va@(T a) vb@(T b) (cont:tail) | get cont == [a] = (insert [b] cont:tail)
                                   | get cont == [b] = (insert [a] cont:tail)
                                   | otherwise       = case (aval, bval) of 
                                                                (True, True)   -> (cont:pair va vb tail)
                                                                (True, False)  -> (remove a cont:pair va vb tail)
                                                                (False, True)  -> (remove b cont:pair va vb tail)
                                                                (False, False) -> ((remove a $ remove b cont):pair va vb tail)
                                                              where   aval = elem a $ get cont
                                                                      bval = elem b $ get cont
                                            
pair va@(T a) vb@(F b) (cont:tail) | get cont == [a] = (remove b cont: pair va vb tail)
                                   | get cont == [b] = (remove a cont: pair va vb tail)
                                   | otherwise       = (cont: pair va vb tail)
                                            
pair va@(F a) vb@(T b) (cont:tail) | get cont == [a] = (remove b cont: pair va vb tail)
                                   | get cont == [b] = (remove a cont: pair va vb tail)
                                   | otherwise       = (cont: pair va vb tail)

pair va@(F a) vb@(F b) (cont:tail) | notInCont a = (remove b cont:tail)
                                   | notInCont b = (remove a cont:tail)
                                   | otherwise   = (cont: pair va vb tail)
                                     where notInCont el = not $ elem el $ get cont
                                     
--Remains a single table.
relative :: (Contained a, Eq a, Contained b, Eq b) 
                => Val a -> Val b -> Int -> [Container] -> [Container]
relative _ _ _ [] = []
relative va@(T a) vb@(T b) offset week | offset == 0 = pair va vb week
                                       | offset > 0  = let notOverlap = applyFunctionList (map (\x -> direct (F b) x) [0..(offset-1)]) week
                                                           in relCheck notOverlap
                                           where
                                               applyFunctionList [] a       = a
                                               applyFunctionList (f:tail) a = f (applyFunctionList tail a)
                                               relCheck [] = []
                                               relCheck (cont:tail) | length tail < offset = (remove a cont:tail)
                                                                    | get cont == [a] = (cont:direct (T b) (offset-1) tail)
                                                                    | otherwise       = if aval <= bval
                                                                                            then if bval <= aval
                                                                                                    then (cont: relCheck tail)
                                                                                                    else (cont:(relCheck $ direct (F b) (offset-1) tail))
                                                                                            else (remove a cont:relCheck tail)
                                                                                                where   aval = elem a $ get cont
                                                                                                        bval = elem b $ get (tail !! (offset - 1))
                                                                                                       

directs = direct (T Anne) 0 . direct (T Stan) 2 . direct (T Rob) 4
pairs = pair (F Wally) (T Anne) . pair (T Vern) (T Fran) . pair (T Rob) (F Ida)
relatives = relative (T Eve) (T Vern) 1

solve directs pairs relatives week = let afterDirects = directs week
                                         in iterate afterDirects
                                         where iterate week = let after = propagateSolved $ relatives $ pairs week
                                                                  in if after == week
                                                                      then after
                                                                      else iterate after
                                                                  
solveMarriage = solve directs pairs relatives theweek